Rails.application.routes.draw do
  get 'products' => 'products#index'
  get 'products/:id' => 'products#show'
  get 'categories' => 'categories#index'
  get 'categories/:id' => 'categories#show'
  get 'categories/:id/products' => 'categories#products'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
