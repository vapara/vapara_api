class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :products]

  # GET /categories
  def index
    @categories = Category.order(:title)

    render json: @categories
  end

  # GET /categories/1
  def show
    render json: @category
  end

  # GET /categories/1/products
  def products
    products = Product.where(category_id: @category.id).order(:title)
    products_with_images = products.map { |product| infos(product) }
    render json: products_with_images
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_category
    @category = Category.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def category_params
    params.require(:category).permit(:title, :dolibarr_id)
  end
end
