class ApplicationController < ActionController::API
  def infos(product)
    { id: product.id,
      ref: product.ref,
      category_id: product.category_id,
      category_title: product.category.title,
      title: product.title,
      description: product.description,
      url_supplier: product.url_supplier,
      stock: product.stock,
      second_hand: product.second_hand,
      price: product.price,
      images: product.images_url }
  end
end
