class ProductsController < ApplicationController
  # GET /products
  def index
    products = Product.all

    products_with_images = products.map { |product| infos(product) }

    render json: products_with_images
  end

  # GET /products/1
  def show
    product = Product.find(params[:id])
    render json: infos(product)
  end
end
