class Product < ApplicationRecord
  include Rails.application.routes.url_helpers

  belongs_to :category
  validates :title, presence: true
  validates :ref, presence: true
  validates :ref, uniqueness: true

  def images_url
    image = []
    Dir.foreach(File.join('public/images/', id.to_s)) do |x| 
      image << "/images/#{id}/#{x}" unless ['..', '.'].include?(x)
    end
    image.sort
  end
end
