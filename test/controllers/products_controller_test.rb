require 'test_helper'

class ProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product = products(:one)
  end

  test "should get index" do
    get products_url, as: :json
    assert_response :success
  end

  test "should create product" do
    assert_difference('Product.count') do
      post products_url, params: { product: { category_id: @product.category_id, description: @product.description, dolibarr_id: @product.dolibarr_id, price: @product.price, ref: @product.ref, second_hand: @product.second_hand, stock: @product.stock, title: @product.title, url_supplier: @product.url_supplier } }, as: :json
    end

    assert_response 201
  end

  test "should show product" do
    get product_url(@product), as: :json
    assert_response :success
  end

  test "should update product" do
    patch product_url(@product), params: { product: { category_id: @product.category_id, description: @product.description, dolibarr_id: @product.dolibarr_id, price: @product.price, ref: @product.ref, second_hand: @product.second_hand, stock: @product.stock, title: @product.title, url_supplier: @product.url_supplier } }, as: :json
    assert_response 200
  end

  test "should destroy product" do
    assert_difference('Product.count', -1) do
      delete product_url(@product), as: :json
    end

    assert_response 204
  end
end
