# frozen_string_literal: true

namespace :dolibarr do
  CONF_FILE = Rails.root.join('config', 'dolibarr.yml')
  CONF = YAML.load_file(CONF_FILE) if File.exist?(CONF_FILE)

  desc 'Populate all datas from dolibarr'
  task sync_global: :environment do
    sync_categories
    sync_products
  end

  desc 'Populate Categories from dolibarr'
  task sync_categories: :environment do
    sync_categories
  end

  desc 'Populate Products from dolibarr'
  task sync_products: :environment do
    sync_products
  end
end

def sync_categories
  dol_categories = requete_dolibarr('categories?type=product')
  dol_categories.each do |dol_category|
    puts "Create or modify category : #{dol_category[:label]}"
    category = Category.where(dolibarr_id: dol_category[:id]).first_or_initialize
    category.update!(title: dol_category[:label])
  end
  delete_categories(dol_categories)
end

def delete_categories(dol_categories)
  dol_cat_ids = dol_categories.map { |c| c[:id].to_i }
  categories = Category.all.select { |cat| dol_cat_ids.exclude?(cat.dolibarr_id) }
  puts "Destroy #{categories.count} old categories"
  categories.each &.destroy unless categories.empty?
end

def sync_products
  Dir.mkdir('public/images') unless Dir.exist?('public/images')

  dol_product_to_sell_ids = []
  categories = Category.where.not(dolibarr_id: nil)
  categories.each do |category|
    dol_products = requete_dolibarr("categories/#{category.dolibarr_id}/objects?type=product")
    next unless dol_products

    dol_product_to_sell_ids += sync_product_from_category(category, dol_products)
  end
  delete_products(dol_product_to_sell_ids)
end

def sync_product_from_category(category, dol_products)
  dol_product_to_sell_ids = []
  dol_product_to_sell = dol_products.select { |prod| option(prod, 'vapara') == '1' }
  dol_product_to_sell.each do |dol_product|
    product = create_or_update_product(category, dol_product)
    create_or_update_images(product)
    dol_product_to_sell_ids << dol_product[:id].to_i
  end
  dol_product_to_sell_ids
end

def delete_products(dol_prod_ids)
  products = Product.all.select { |prod| dol_prod_ids.exclude?(prod.dolibarr_id) }
  puts "Destroy #{products.count} old products"
  products.each { |product| product.destroy }
end

def create_or_update_product(category, dol_product)
  puts "Create or modify product : #{dol_product[:label]}"
  product = Product.where(dolibarr_id: dol_product[:id]).first_or_initialize
  product.update!(title: dol_product[:label],
                  description: dol_product[:description],
                  category_id: category.id,
                  stock: dol_product[:stock_reel],
                  url_supplier: dol_product[:url],
                  ref: dol_product[:ref],
                  second_hand: option(dol_product, 'occasion') == '1',
                  price: (dol_product[:price].to_f * CONF['factor']).round(2))
  product
end

def create_or_update_images(product)
  puts "Add images for #{product.title}"
  `rm -Rf #{File.join(Rails.root, 'public/images/', product.id.to_s)}`
  Dir.mkdir(File.join('public/images/', product.id.to_s))
  dol_documents = requete_dolibarr("documents?modulepart=product&id=#{product.dolibarr_id}&sortfield=name")
  return unless dol_documents.is_a?(Array)

  dol_documents.each do |dol_document|
    dol_file = requete_dolibarr("documents/download?modulepart=product&original_file=\
                                #{dol_document[:level1name]}%2F#{dol_document[:relativename]}")
    next unless dol_file && ['image/jpeg', 'image/png'].include?(dol_file[:"content-type"])

    create_image_from_document(dol_file, product)
  end
end

def create_image_from_document(dol_file, product)
  temp_file = File.join('public/images/', product.id.to_s, dol_file[:filename])
  File.open(temp_file, 'wb') do |f|
    f.write(Base64.decode64(dol_file[:content]))
  end
end

def requete_dolibarr(requete)
  result = RestClient.get "#{CONF['url']}/api/index.php/#{requete}",
                          params: { DOLAPIKEY: CONF['dolapikey'] }
  eval(result.body.gsub('null', 'nil'))
rescue StandardError
  nil
end

def option(object, option_name)
  object && object[:array_options] &&
    object[:array_options].is_a?(Hash) &&
    object[:array_options]["options_#{option_name}".to_sym]
end
