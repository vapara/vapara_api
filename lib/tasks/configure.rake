# frozen_string_literal: true

namespace :configure do
  DOLIBARR_CONF_FILE = Rails.root.join('config', 'dolibarr.yml')
  desc 'Create dolibarr.yml config file'
  task dolibarr: :environment do
    puts 'Préparation de la liaison avec Dolibarr'
    conf = File.exist?(DOLIBARR_CONF_FILE) ? YAML.load_file(DOLIBARR_CONF_FILE) : {}
    puts "Url de Dolibarr [#{conf['url']}] : "
    url = STDIN.gets.chomp
    conf['url'] = url unless url.empty?
    puts "Dolapikey [#{conf['dolapikey']}] : "
    dolapikey = STDIN.gets.chomp
    conf['dolapikey'] = dolapikey unless dolapikey.empty?
    puts "Facteur multiplicateur pour les prix en June [#{conf['factor']}] : "
    factor = STDIN.gets.chomp.to_f
    conf['factor'] = factor
    File.write(DOLIBARR_CONF_FILE, conf.to_yaml)
    puts 'Configuration terminée'
  end
end
