class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :title
      t.text :description
      t.references :category, null: false, foreign_key: true
      t.integer :stock
      t.integer :dolibarr_id
      t.string :url_supplier
      t.string :ref
      t.boolean :second_hand, default: false
      t.float :price

      t.timestamps
    end
  end
end
